// include the required packages. 
//var gulp = require('gulp');
var watchify = require('watchify');
var stylus = require('gulp-stylus');
var browserify = require('browserify');
var gulp = require('gulp');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var gutil = require('gulp-util');
var sourcemaps = require('gulp-sourcemaps');
var assign = require('lodash.assign');

//const babel = require('gulp-babel');
// add custom browserify options here
var customOpts = {
  entries: ['public/js/index.js'],
  debug: true
};
var opts = assign({}, watchify.args, customOpts);
var b = watchify(browserify(opts)); 
b.transform('brfs');

// Get one .styl file and render 
gulp.task('main', function () {
  gulp.src('public/stylus/main.styl')
    .pipe(stylus({
      compress: true
    }))
    .pipe(gulp.dest('public/css'));
});


gulp.task('js', bundle); // so you can run `gulp js` to build the file
b.on('update', bundle); // on any dep update, runs the bundler
b.on('log', gutil.log); // output build logs to terminal

function bundle() {
  return b.bundle()
    // log errors if they happen
    .on('error', gutil.log.bind(gutil, 'Browserify Error'))
    .pipe(source('bundle.js'))
    // optional, remove if you don't need to buffer file contents
    .pipe(buffer())
//    .pipe(babel({
//            presets: ['es2015']
//        }))
    // optional, remove if you dont want sourcemaps
    .pipe(sourcemaps.init({loadMaps: true})) // loads map from browserify file
       // Add transformation tasks to the pipeline here.
    .pipe(sourcemaps.write('public/js/')) // writes .map file
    .pipe(gulp.dest('public/js/dist'));
}

gulp.task('watch', function() {
  gulp.watch('public/stylus/main.styl', ['main']);
//  gulp.watch('public/js/**/*.js', ['js']);
});
// Default gulp task to run 
gulp.task('default', ['main','js','watch']);
 

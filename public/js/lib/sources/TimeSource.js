module.exports = (function() {
    'use strict';

    var async = require('../async');

    function TimeSource(actions) {
        var source = {
            add: function(data) {
                return new Promise(function (resolve, reject) {
                    async("POST",'times',data)
                    .done(function(resp){
                        resolve(resp.data);
                    }).fail(function(resp){
                        reject(resp.responseJSON);
                    });
                });
            },
            update: function(data) {
                return new Promise(function (resolve, reject) {
                    async("PUT",'times/'+data.id,data)
                    .done(function(resp){
                        resolve(resp.data);
                    }).fail(function(resp){
                        reject(resp.responseJSON);
                    });
                });
            },
            fetch: function () {
                // returning a Promise because that is what fetch does.
                return new Promise(function (resolve, reject) {
                    async("GET",'times',{})
                    .done(function(resp){
                        resolve(resp.data.reverse());
                    }).fail(function(resp){
                        reject(resp.responseJSON);
                    });
                });
            },
            delete: function(id) {
                return new Promise(function (resolve, reject) {
                    async("DELETE",'times/'+id)
                    .done(function(resp){
                        resolve(id);
                    }).fail(function(resp){
                        reject(resp.responseJSON);
                    });
                });
            },
        }
        return source;
    }
    return TimeSource;
  })();
module.exports = (function() {
    'use strict';
    
    var async = require('../async'); 
    
    function SubprojectSource() {
        var Source = {
            add: function(data) {
                return new Promise(function (resolve, reject) {
                    async("POST",'subprojects',data)
                    .done(function(resp){
                        resolve(resp.data);
                    }).fail(function(resp){
                        reject(resp.responseJSON);
                    });
                });
            },
            update: function(data) {
                return new Promise(function (resolve, reject) {
                    async("PUT",'subprojects/'+data.id,data)
                    .done(function(resp){
                        resolve(resp.data);
                    }).fail(function(resp){
                        reject(resp.responseJSON);
                    });
                });
            },
            delete: function(id) {
                return new Promise(function (resolve, reject) {
                    async("DELETE",'subprojects/'+id)
                    .done(function(){
                        resolve(id);
                    }).fail(function(resp){
                        reject(resp.responseJSON);
                    });
                });
            },
        };
        return Source;
    }
    return SubprojectSource;
})();
module.exports = (function() {
    'use strict';
    
    var async = require('../async'); 
    
    function ProjectSource() {
        var Source = {
            add: function(data) {
                return new Promise(function (resolve, reject) {
                    async("POST",'projects',data)
                    .done(function(resp){
                        resolve(resp.data);
                    }).fail(function(resp){
                        reject(resp.responseJSON);
                    });
                });
            },
            update: function(data) {
                return new Promise(function (resolve, reject) {
                    async("PUT",'projects/'+data.id,data)
                    .done(function(resp){
                        resolve(resp.data);
                    }).fail(function(resp){
                        reject(resp.responseJSON);
                    });
                });
            },
            fetch: function () {
                return new Promise(function (resolve, reject) {
                    async("GET",'projects',{})
                    .done(function(resp){
                        resolve(resp.data);
                    }).fail(function(resp){
                        reject(resp.responseJSON);
                    });
                });
            },
            delete: function(id) {
                return new Promise(function (resolve, reject) {
                    async("DELETE",'projects/'+id)
                    .done(function(){
                        resolve(id);
                    }).fail(function(resp){
                        reject(resp.responseJSON);
                    });
                });
            },
        };
        return Source;
    }
    return ProjectSource;
})();
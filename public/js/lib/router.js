module.exports = (function() {
    'use strict';
    var Ractive = require('ractive');

var Router = Ractive.extend({  
    template: '<router-handler/>',
    components: {
        'router-handler': function() {
            return this.get('componentName');
        }
    },
    oninit: function() {
        this.observe('componentName', function(newValue, oldValue) {
            if (this.fragment.rendered) {
                this.reset();
            }
        });
    }
});

return Router;
})();
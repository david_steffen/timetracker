module.exports = (function() {
    'use strict';

    var flux = require('../flux');
    
    function TimeActions(sources) {
        return flux.createActions({
            addItem : function (item) {
//                this.dispatch();
                var self = this;
                sources.add(item)
                .then(function(time) {
                    self.dispatch(time);
                })
                .catch(function(errorMessage) {
                    self.actions.timesFailed(errorMessage);
                });

            },
            updateItem : function (item) {
                var self = this;
                sources.update(item)
                .then(function(time) {
                    self.dispatch(time);
                })
                .catch(function(errorMessage) {
                    self.actions.timesFailed(errorMessage);
                });

            },
            
            fetchItems: function() {
                // we dispatch an event here so we can have "loading" state.
//                this.dispatch();
                var self = this;
                sources.fetch()
                .then(function(times) {            
                    // we can access other actions within our action through `this.actions`
                    self.dispatch(times);
                })
                .catch(function(errorMessage) {
                    self.actions.timesFailed(errorMessage);
                });

            },
            deleteItem: function(id){
                var self = this;
                sources.delete(id)
                .then(function(id) {            
                    // we can access other actions within our action through `this.actions`
                    self.dispatch(id)
                })
                .catch(function(errorMessage) {
                    self.actions.timesFailed(errorMessage);
                });
            },
            timesFailed: function(errorMessage) {
                this.dispatch(errorMessage);
            }
        });
    }
    return TimeActions;

})();
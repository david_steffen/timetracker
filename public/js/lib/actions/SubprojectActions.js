module.exports = (function() {
    'use strict';

    var flux = require('../flux');

    function SubprojectActions(sources) {
        return flux.createActions({
            addItem : function (item) {
                var self = this;
                sources.add(item)
                .then(function(subproject) {
                    self.dispatch(subproject);
                })
                .catch(function(errorMessage) {
                    console.log(errorMessage)
                    self.actions.projectsFailed(errorMessage);
                });

            },
            updateItem : function (item) {
                var self = this;
                sources.update(item)
                .then(function(subproject) {
                    self.dispatch(subproject);
                })
                .catch(function(errorMessage) {
                    self.actions.projectsFailed(errorMessage);
                });

            },
            deleteItem: function(id){
                var self = this;
                sources.delete(id)
                .then(function(id) {            
                    // we can access other actions within our action through `this.actions`
                    self.dispatch(id)
                })
                .catch(function(errorMessage) {
                    self.actions.projectsFailed(errorMessage);
                });
            },
            projectsFailed: function(errorMessage) {
                this.dispatch(errorMessage);
            }
        });
    }
    return SubprojectActions;

})();
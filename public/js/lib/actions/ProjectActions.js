module.exports = (function() {
    'use strict';

    var flux = require('../flux');

    function ProjectActions(sources) {
        return flux.createActions({
            addItem : function (item) {
//                this.dispatch();
                var self = this;
                sources.add(item)
                .then(function(time) {
                    self.dispatch(time);
                })
                .catch(function(errorMessage) {
                    self.actions.projectsFailed(errorMessage);
                });

            },
            updateItem : function (item) {
                var self = this;
                sources.update(item)
                .then(function(time) {
                    self.dispatch(time);
                })
                .catch(function(errorMessage) {
                    self.actions.projectsFailed(errorMessage);
                });

            },
            
            fetchItems: function() {
                // we dispatch an event here so we can have "loading" state.
//                this.dispatch();
                var self = this;
                sources.fetch()
                .then(function(projects) {            
                    // we can access other actions within our action through `this.actions`
                    self.dispatch(projects);
                })
                .catch(function(errorMessage) {
                    self.actions.projectsFailed(errorMessage);
                });

            },
            deleteItem: function(id){
                var self = this;
                sources.delete(id)
                .then(function(id) {            
                    // we can access other actions within our action through `this.actions`
                    self.dispatch(id)
                })
                .catch(function(errorMessage) {
                    self.actions.projectsFailed(errorMessage);
                });
            },
            selectProject: function(id){
                this.dispatch(id);
            },
            projectsFailed: function(errorMessage) {
                this.dispatch(errorMessage);
            }
        });
    }
    return ProjectActions;

})();
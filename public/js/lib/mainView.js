module.exports = (function() {
    'use strict';

    var fs = require('fs');
    var Ractive = require('ractive');
    var page = require('page');
    var timesComponent = require('../components/times');
    var loginComponent = require('../components/login');
    var projectsComponent = require('../components/projects');
    var projectFormComponent = require('../components/projectform');
    var async = require('./async');
    var $ = require('jquery');
    var template = fs.readFileSync(__dirname + '/../views/mainview.html');
    var subprojectFormComponent = require('../components/subprojectform');
    var projectDropdownComponent = require('../components/projectdropdown');
    var timerComponent = require('../components/timer');
    
    function mainView(container, stores, actions) {
        var view = new Ractive({
            el: container,
            template: template.toString(),
            magic:true,
            data: {
                projectStore: null,
                timeStore: null,
                signedIn: false,
                page: 'login',
                user: null,
            },
            components: {
                Times: timesComponent(actions),
                Login: loginComponent(),
                Projects: projectsComponent(actions),
                ProjectForm: projectFormComponent(actions),
                SubprojectForm: subprojectFormComponent(actions),
//                Router: RouterComponent
            },
            
            events: {
                tap: require( 'ractive-events-tap' )
            },
            transitions: {
                fade: require( 'ractive-transitions-fade' )
            },
            oninit: function oninitHandler() {
                this.set({projectStore: stores.projects.getState()});
                this.set({timeStore: stores.times.getState()});
                this.on({
                    toggleLogin: function() {
                        this.toggle('loginOpen');
                    },
                    toggleMenu: function() {
                        this.toggle('menuOpen');
                    }
                })
            },
            getUser: function() {
            var self = this;
            async("GET",'user')
            .done(function(resp) {
                self.set('user',resp.data);
            }).fail(function(resp){
    //            self.setState({error: true});
    //            self.setState({err_msg: _data.responseJSON.payload.err_msg});
            });
        },
        });
        Ractive.components.ProjectDropdown = projectDropdownComponent(actions);
        Ractive.components.Timer = timerComponent(actions);
        var routes = require('./routes')(page, view, stores, actions);
    
        page.base('/');
        page('*', function(ctx,  next){
            if (ctx.init) {
                next();
            } else {
                $('#content').addClass('transition');
                setTimeout(function(){
                    $('#content').removeClass('transition');
                    next();
                }, 300);
            }
        });
        page('', routes.gotologin);
        page('/', routes.gotologin);
        page('login', routes.login);
        page('times', routes.times);
        page('projects',routes.projects);
        page('logout',routes.logout);
        page('*', routes.notfound);
        page();
        return view;
    }
    
    
    return mainView;
})();
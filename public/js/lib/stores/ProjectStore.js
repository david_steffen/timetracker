module.exports = (function ProjectStoreModule() {
    'use strict';
    
    
    var flux = require('../flux');
    var extend = require('extend');


    function ProjectStore(actions) {
        return flux.createStore({
            displayName: 'ProjectStore',

            bindListeners: {
                addItem: actions.projects.addItem,
                updateItem: actions.projects.updateItem,
                loadItems: actions.projects.fetchItems,
                deleteItem: actions.projects.deleteItem,
                setError: actions.projects.PROJECTS_FAILED,
                selectProject: actions.projects.SELECT_PROJECT,
                addSubItem: actions.subprojects.addItem,
                updateSubItem: actions.subprojects.updateItem,
                deleteSubItem: actions.subprojects.deleteItem,
            },
            state: {
                projects: null,
                errorMessage: null,
                selected_project:0,
                graphColours:{},
                graphPoints:{},
                
            },
            publicMethods: {
                hasProjects: function () {
                    return !!this.getState().projects;
                },
            },
            setError: function (error) {
                this.errorMessage = error;
                this.setState({errorMessage:error})
            },
            addItem: function (item) {
                var items = this.state.projects;
                items[item.id] = item;
                this.setState({projects: items});

            },
            updateItem: function(item) {
                var items = this.state.projects;
                items[item.id] = item;
                this.setState({projects: items});
            },
            loadItems: function(projects) {
                this.selectProject(projects[0].id);
                this.setState({projects: this.formatItems(projects)});
            },
            deleteItem: function(id) {
                var items = this.state.projects;
                delete items[id];
                this.setState({projects: items});
            },
            selectProject: function(id){
                this.setState({selected_project :id});
            },
            setGraphData: function(item) {
                var point = parseFloat(item.time_this_week)
                if(point) {
                    var graphColours = this.state.graphColours;
                    var graphPoints = this.state.graphPoints;
                    graphColours[item.abbreviation] = item.colour;
                    graphPoints[item.abbreviation] = point;
                    this.setState({graphColours: graphColours, graphPoints: graphPoints});
                }
            },
            formatItem: function(items,item) {
                items[item.id]=item;
                var subprojects = item.subprojects.data,new_subprojects = {};
                for(var y in subprojects) {

                    new_subprojects[ subprojects[y].id ] = subprojects[y];
                }
                items[ item.id ].subprojects = new_subprojects;
                return items;
            },
            formatItems: function(response) {
                var projects ={};
                for(var x in response) {
                    var item = response[x];
                    this.setGraphData(item);
                    projects = this.formatItem(projects,item);
                }
                return projects;
            },
            addSubItem: function (item) {
                var items = this.state.projects;
                var project_id = this.state.selected_project;
                console.log(project_id);
                items[project_id].subprojects[item.id] = item;
                this.setState({projects: items});
            },
            updateSubItem: function(item) {
                var items = this.state.projects;
                var project_id = this.state.selected_project;
                items[project_id].subprojects[item.id] = item;
                this.setState({projects: items});
            },
            deleteSubItem:  function(id) {
                var items = this.state.projects;
//                delete items[id];
                this.setState({projects: items});
            },
        });
    }
    return ProjectStore;
})();
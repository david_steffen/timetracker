module.exports = (function TimeStoreModule() {
    'use strict';
    
    
    var flux = require('../flux');
    var moment = require('moment');
    var _ = require('underscore');
    function TimeStore(actions) {
        return flux.createStore({
            displayName: 'TimeStore',
            bindListeners: {
                addItem: actions.addItem,
                updateItem: actions.updateItem,
                loadItems: actions.fetchItems,
                deleteItem: actions.deleteItem,
                setError: actions.timesFailed,
            },
            state: {
                times: [],
                errorMessage: null,
                logDates: null,
                timer: 0,
                timerInit: false,
            },

            publicMethods: {
                hasTimes: function () {
                    return !!this.state.times.length;
                },
                totalItems: function() {
                    return this.state.times.length;
                }
            },
            setError: function (error) {
                this.setState({errorMessage: error});
            },
            addItem: function (item) {
                var items = this.state.times, 
                    added = false,
                    date_format = 'X',
                    item_date = moment(item.log_date).format(date_format);
                for(var x in items) {
                    var list_item_date = moment(items[x].log_date).format(date_format);
                    if(list_item_date < item_date){
                        items.splice(x, 0, item);
                        added = true;
                        break;
                    } 
                }
                if(!added){
                    items.push(item);
                }
                this.loadItems(items);
            },
            updateItem: function(item) {
                var items = this.state.times,actions = this.actions;
                for(var x in items) {
                    if(items[x].id === item.id) {
                        if(items[x].log_date !== item.log_date) {
                            
                            items.splice( x, 1);
                            this.addItem(item);
                            return;
                        } else {
                            items[x] = item;
                        }
                        break;
                    }
                }
                actions.loadItems(items);
            },
            loadItems: function(times) {
                this.setState({logDates: this.findDates(times),times: times});
            },
            deleteItem: function(id) {
                var items = this.state.times;
                for(var x in items) {
                    if(items[x].id === id) {
                        items.splice( x, 1);
                        break;
                    }
                }
                this.loadItems(items);
            },
            findDates: function(items) {
                var logDates={};
                for(var x in items) {
                    var date = moment(items[x].log_date).startOf('week').format('X');
                    if(!Array.isArray(logDates[ date ])) {
                        logDates[ date ]= [];
                    }
                    logDates[ date ].push(items[x].id)
                }
                return logDates;

            },
        });
    }
    return TimeStore;
})();
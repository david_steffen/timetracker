module.exports = (function() {
    'use strict';
    
       
    function routes(page, ractive, stores, actions){
        function isLoggedIn() {
            var expiry_date = parseInt(localStorage.getItem("expiry_date"));
            var dateObj = new Date();
            var current_date = parseInt((dateObj.getTime()) / 1000);
            if(expiry_date > current_date) {
                ractive.set('signedIn',true);
                if(!ractive.get('user')) {
                    ractive.getUser();
                }
                if(!stores.projects.hasProjects()) {
                    actions.projects.fetchItems();
                }
                if(!stores.times.hasTimes()) {
                    actions.times.fetchItems();
                }
                return true;
            } else {
                ractive.set('signedIn',false);
                return false;
            }
        }
        var routeActions = {
            gotologin: function () {
                if(isLoggedIn()) {
                    page.redirect('times');
                } else {
                    page.redirect('login');
                }
            },
            login: function() {
                if(isLoggedIn()) {
                    page.redirect('times');
                } else {
                    ractive.set('page','login');
                }
            },
    
            times: function() {

                if(isLoggedIn()) {
                    ractive.set('page','times');
                } else {
                    page.redirect('login');
                }
            },
            projects: function() {
                if(isLoggedIn()) {
                    ractive.set('page','projects');
                } else {
                    page.redirect('login');
                }
            },
            logout: function()
            {
                    ractive.set('signedIn',false);
                    localStorage.setItem("token",null);
                    localStorage.setItem("expiry_date",null);
                    ractive.set('page','login');
                    page.redirect('/');
            },
            notfound: function()
            {
                if(isLoggedIn()) {
                    ractive.set('page','notfound');
                }
            }
        };
        return routeActions;
    }
    return routes;
})()

module.exports = (function() {
    'use strict';
    var $ = require('jquery');
    function async(type,url,data) {
        var domain = "/api/"+url;
        var token = window.localStorage.getItem("token");        
        return $.ajax({type: type,url:domain,data:data,headers: {
            "Authorization": "Bearer " + token
        },});
    };
    return async;
})();


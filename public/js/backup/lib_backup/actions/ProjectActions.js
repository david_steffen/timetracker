define(['flux','async'],function(flux,async){

    var ProjectActions = flux.createActions({
        addItem : function (item) {
            var self = this;
            async("POST","projects",item)
            .done(function(resp){
                var project = resp.data;
                return project;
            }).fail(function(resp){
                self.projectsFailed(resp.responseJSON);
            });
            
        },
        updateItem : function (item) {
            var self = this;
            async("PUT","projects/"+item.id,item)
            .done(function(resp){
                var project = resp.data;
                return project;
            }).fail(function(resp){
                self.projectsFailed(resp.responseJSON);
            });
            
        },
        loadItems: function(projects){
            return projects;
        },
        fetchProjects: function() {
            // we dispatch an event here so we can have "loading" state.
            var self = this
            async("GET",'projects',{})
            .done(function(resp){
                self.loadItems(resp.data)
            }).fail(function(resp){
                self.projectsFailed(resp.responseJSON);
            });

        },
        
        selectProject: function(id){
            return id;
        },
        projectsFailed: function(errorMessage) {
            return errorMessage;
        }
    });
    return ProjectActions;
});
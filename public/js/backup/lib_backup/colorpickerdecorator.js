define(['jquery','minicolors'],function($, colorPicker){
    
    return function(node,colour){
        $(node).val(colour)
       $(node).minicolors({theme: 'custom',color: colour});
        return {
            update: function() {
                console.log(colour)
              $(node).minicolors({theme: 'custom',color: colour}); 
            },
            teardown: function () {
                $(node).minicolors('destroy');
            }
        };
    };
});
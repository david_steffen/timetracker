define(['jquery'],function($){
    return function(type,url,data) {
        var domain = "http://timelara.local/api/v1/"+url;
        var token = window.localStorage.getItem("token");        
        return $.ajax({type: type,url:domain,data:data,headers: {
            "Authorization": "Bearer " + token
        },});
    };
});


define(function(require){
    var flux = require('flux');
    var projectActions = require('actions/ProjectActions');
    var ProjectStore = flux.createStore({
        displayName: 'ProjectStore',

        bindListeners: {
            addItem: projectActions.addItem,
            updateItem: projectActions.updateItem,
            loadItems: projectActions.loadItems,
            setError: projectActions.projectsFailed,
            selectProject: projectActions.selectProject,
        },
        state: {
            projects: [],
            errorMessage: null,
            selected_project:0,
        },

        publicMethods: {
            hasProjects: function () {
                return !!this.getState().projects.length;
            },
        },
        setError: function (error) {
            this.errorMessage = error;
            this.emitChange();
        },
        addItem: function (item) {
            console.log('add')
            var items = this.state.projects;
            items.push(item);
            this.setState({projects: items});
//            this.emitChange();
            
        },
        updateItem: function(item) {
            
            console.log('update')
            var items = this.state.projects;
            for(var x in items) {
                if(items[x].id === item.id) {
                    items[x] = item;
//                    this.splice( 'times', x, 1, time );
                    break;
                }
            }
            this.setState({projects: items});
        },
        loadItems: function(projects) {
            this.state.projects = projects;
            this.emitChange();
        },
        selectProject: function(id){
            this.state.selected_project = id;
            this.emitChange();
        },
        formatItems: function(response) {
            var projects = {};
            this.selectProject(response[0].id);
            for(x in response) {
                projects[ response[x].id ] = response[x];
                var subprojects = response[x].subprojects.data,new_subprojects = {};
                for(y in subprojects) {

                    new_subprojects[ subprojects[y].id ] = subprojects[y];
                }
                projects[ response[x].id ].subprojects = new_subprojects;
            }
            return projects;
        }
    });
    return ProjectStore;
});
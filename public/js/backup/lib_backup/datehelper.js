var DateHelper = function() {
    this.dateObj;
};

    DateHelper.setDateObject = function(dateObj) 
    {
        this.dateObj = dateObj;
    };
    DateHelper.getDateObject = function() 
    {
        return this.dateObj;
    };
    DateHelper.recentDays = function(dateObj) 
    {
        var currentDay = new Date();
        var date = new Date(dateObj);
        var dateFormatted = this.defaultFormat(date);
        if(this.defaultFormat(currentDay) === dateFormatted) {
            return 'Today';
        } else if(this.defaultFormat(currentDay.setDate(currentDay.getDate() -1)) === dateFormatted) {
            return 'Yesterday';
        } else {
            return dateFormatted;
        }
    };
    DateHelper.defaultFormat = function(dateObj) 
    {   
        var dateObj = new Date(dateObj);
        var day = dateObj.getDate();
        if(day < 10) {day = '0'+day; }
        var month = dateObj.getMonth()+1;
        if(month < 10) { month = '0'+month; }
        var year = dateObj.getFullYear()
        var formatDate = day+'/'+month+'/'+year;
        return formatDate;
    };
    DateHelper.isJSTimestamp = function(timestamp) 
    {
        var obj = new Date(timestamp);
        var time = obj.getTime();
        return !isNaN(time);
        //return time === time;
    };
    DateHelper.createJSTimestamp = function(unix_timestamp) 
    {
//        var offset = (new Date().getTimezoneOffset())*3600;
        //var invertedOffset = offset *= -1;
        return (unix_timestamp*1000);
    };
    DateHelper.createUnixTimestamp = function(js_timestamp) 
    {
        return Math.floor(((parseInt(js_timestamp))/1000)).toString();
    };
    DateHelper.getTimestampString = function(dateObj) 
    {
        return (new Date(dateObj).getTime());
    };
    DateHelper.getPreviousDates = function(dateObj, count) 
    {
        var array = [];
        for(var x = 0; x < count; x++) {
            array.push({formattedDate: this.recentDays(dateObj), timestamp: this.getTimestampString(dateObj)});
            dateObj.setDate(dateObj.getDate() - 1);
        }
        return array.reverse();
    };
    DateHelper.getNextDates = function(dateObj, count) 
    {
        var array = [];
        for(var x = 0; x < count; x++) {
            dateObj.setDate(dateObj.getDate() + 1);
            array.push({formattedDate: this.recentDays(dateObj), timestamp: this.getTimestampString(dateObj)});
        }
        return array;
    };

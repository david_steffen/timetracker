define(function (require) {
    'use strict';

    var mainTemplate = require('rvc!ui/maintemplate');
    var async = require('async');
    var page = require('page');
    var DateHelper = require('datehelper');
    var moment = require('moment');
//    var Alt = require('alt');
////    console.log(Alt)
//    var alt = new Alt();
//    var Ractive = require('ractive');
    var ProjectStore = require('stores/ProjectStore');
    var ProjectActions = require('actions/ProjectActions');


    // render our main view
    var template = new mainTemplate({
        el:'body',
        magic: true, 
        data: {
            signedIn: false,
            page: null
        },
        events: {
            tap: require( 'ractive-events-tap' )
        },
        transitions: {
            fade: require( 'ractive-transitions-fade' )
        },
    });

ProjectStore.listen(function (state) {
    console.log(state)
//    console.log(template.get('projects'))
});  
//    console.log(alt)
    function isLoggedIn() {
        var token = localStorage.getItem("token");
         if(token && token.length > 10) {
//            template.set('signedIn',true);
//            if(!template.get('user')) {
//                template.getUser();
//            }
            if(!ProjectStore.hasProjects()) {
//                console.log(projectActions)
//                projectActions.addItem('mooo')
                    ProjectActions.fetchProjects();
            }
            return true;
        } else {
           return false;
        }
    }
    
    page.base('/');
//    page('*', function(ctx,  next){
//        if (ctx.init) {
//          next();
//        } else {
//            console.log('here');
//          content.classList.add('transition');
//          setTimeout(function(){
//            content.classList.remove('transition');
//            next();
//          }, 300);
//        }
//      })
    page('', gotologin);
    page('/', gotologin);
    page('login', login);
    page('times', times);
    page('projects',projects);
    page('logout',logout);
    page('*', notfound);
    page();
    
    function gotologin() {
        if(isLoggedIn()) {
            page.redirect('times');
//            template.set('page','times');
        } else {
            page.redirect('login');
        }
    }
    function login() {
        if(isLoggedIn()) {
            page.redirect('times');
//            template.set('page','times');
        } else {
            console.log(template)
            template.set('page','login');
        }
    }
    
    function times() {
        
            console.log(template)
        if(isLoggedIn()) {
            template.set('page','times');
        }
    }
    function projects() {
        if(isLoggedIn()) {
            template.set('page','projects');
        }
    }
    function logout()
    {
            template.set('signedIn',false);
            localStorage.setItem("token",null);
            
            template.set('page','login');
            page.redirect('/');
    }
    function notfound()
    {
        if(isLoggedIn()) {
            template.set('page','notfound');
        }
    }
    
});
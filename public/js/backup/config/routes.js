module.exports = (function() {
    'use strict';


var router = require('../lib/router');  
var times from '../components/times';  
//import projects from '../components/projects';

var routes = new Map();

routes.set('/', (context, next) => {  
    next(null, login);
});

routes.set('/times', (context, next) => {  
    next(null, times);
});

export default routes; 
    })();

// First we have to configure RequireJS
require.config({
    baseUrl: 'js/lib',
	// This tells RequireJS where to find Ractive and rvc
	paths: {
        app: '../app',
        ui: '../components'
        
	},
	// These aren't used during development, but the optimiser will
	// read this config when we run the build script
    name: "app",
	out: '../dist/js/app.js',
	stubModules: [ 'rvc' ]
});
//require([ 'rvc!ui/mainTemplate' ], function ( mainTemplate ) {
//        var ractive = new mainTemplate({ 
//            el:'body',
//            magic: true, 
//        });
//    });
// Now we've configured RequireJS, we can load our dependencies and start
require([ 'app/main']);



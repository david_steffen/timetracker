module.exports = (function() {
    'use strict';

    var ProjectActions = require('./lib/actions/ProjectActions');
    var ProjectStore = require('./lib/stores/ProjectStore');
    var TimeActions = require('./lib/actions/TimeActions');
    var TimeStore = require('./lib/stores/TimeStore');
    var TimeSource = require('./lib/sources/TimeSource');
    var ProjectSource = require('./lib/sources/ProjectSource');
    var SubprojectActions = require('./lib/actions/SubprojectActions');
    var SubprojectSource = require('./lib/sources/SubprojectSource');
    var mainView = require('./lib/mainView');
    
    
    function App(container) {
        var timeSource = TimeSource();
        var projectSource = ProjectSource();
        var subprojectSource = SubprojectSource();
        var projectActions = ProjectActions(projectSource);
        var subprojectActions = SubprojectActions(subprojectSource);
        var projectStore = ProjectStore({projects:projectActions,subprojects:subprojectActions});
        var timeActions = TimeActions(timeSource);
        var timeStore = TimeStore(timeActions);
        var view = mainView(container, {
            //stores
            projects: projectStore,
            times: timeStore,
        }, {
            //actions
            projects: projectActions,
            times: timeActions,
            subprojects:subprojectActions
        });
        var removeProjectChangeHandler = projectStore.listen(function listChangeHandler(states) {
            view.set({projectStore: states});
        }.bind(view));
        var removeTimesChangeHandler = timeStore.listen(function listChangeHandler(states) {
            view.set({timeStore: states});
        }.bind(view));
        view.on('teardown', function onteardownHandler() {
            removeProjectChangeHandler();
            removeTimesChangeHandler();
        });
        console.log('app created!');
    }
    return App;

})();
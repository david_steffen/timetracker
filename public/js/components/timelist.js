module.exports = (function() {
    'use strict';
    
    var fs = require('fs');
    var moment = require('moment')
    var Ractive = require('ractive');
    var template = fs.readFileSync(__dirname + '/../views/timelist.html');
    var Immutable = require('immutable');
    var $ = require('jquery');
    
    function timeListComponent(actions) {
        var component = Ractive.extend({
            template: template.toString(),
            data: {
                isLastItem: function(id,log_date,dates) {
                    
                    var systemDate = moment(log_date).startOf('week').format('X');
                    var items = dates[systemDate];
                    if(id == items[0]) {
                        return true;
                    } else {
                        return false;
                    }
                
                },
                formatDate: function(date) {
                    return moment(date).format('ddd Do MMM YYYY');
                },
                startOfWeek: function(date) {
                    return moment(date).startOf('week').format('Do MMM');
//                    return moment(date).format('Do');
                },
                endOfWeek: function(date) {
                    return moment(date).endOf('week').format('Do MMM YYYY');
                },
            },
            computed: {
                
            },
            onrender: function(){
            
            },
            oninit: function() {
                this.on({
                    editTime: function(event) {
                        
                        var timeComponent = this.findParent('Times');
                        var time = Immutable.Map(this.get(event.keypath));
                        var new_time = time.toJS();
                        timeComponent.set({edit: true,open: true,form:true});
                        timeComponent.findComponent('TimeForm').set('time',new_time);
                        var projectDropdown = timeComponent.findComponent('ProjectDropdown');
                        actions.projects.selectProject(new_time.project_id)
                        projectDropdown.set('item_id',new_time.project_id);
                        projectDropdown.set('subproject_id',new_time.subproject_id);
                        timeComponent.findComponent('DayPicker').set('newDate',new_time.log_date);
                    },
                    toggleColumn : function(event){
                        this.toggle('open');
                        if(this.get('edit')) {
                            this.set('edit',false)
                        }
                    },
                    toggleForm: function(event) {
                        if(event.node.dataset.type === 'form') {
                            this.set({form: true});
                        } else {
                            this.set({form: false});
                        }
                    },
                })
            },
        });
        return component;
    }
    return timeListComponent;
})();

module.exports = (function() {
    'use strict';
    
    var fs = require('fs');
//    var async = require('async');
    var moment = require('moment');
    var Ractive = require('ractive');
    var template = fs.readFileSync(__dirname + '/../views/daypicker.html');
    
    function daypickerComponent(actions) {
        var component = Ractive.extend({
            template: template.toString(),
            data: {
                dates: null,
                currentDate: null,
                newDate: null,
                reset: false,
                formatDate: function(date) {
                    return moment(date).format('ddd Do');
                },
                formatMonth: function(date) {
                    return moment(date).format('MMMM');
                },
                formatYear: function(date) {
                     return moment(date).format('YYYY');   
                },
            },
            onchange: function(prop) {

                if(typeof prop.newDate !== 'undefined' && prop.newDate) {
                    var date = prop.newDate;
                    this.set('newDate',null);
                    this.setDates(date);
                }

                if(typeof prop.reset !== 'undefined' && prop.reset) {
                    this.set('currentDate', moment().format());
                }
            },
            oninit: function() {
                this.setDates();
                this.on({
                    selectDate: function(event){
                        this.set('currentDate',event.context)
                    },
                    previousDates: function() {
                        var currentDate = this.get('currentDate');
                        var date = moment(currentDate);
                        this.setDates(date.subtract(7,'d'));
                    },
                    nextDates: function(event) {
                        var currentDate = this.get('currentDate');
                        var date = moment(currentDate);
                        this.setDates(date.add(7,'d'));
                    },
                    previousMonth: function() {
                        var currentDate = this.get('currentDate');
                        var date = moment(currentDate);
                        this.setDates(date.subtract(1,'M'));
                    },
                    nextMonth: function() {
                        var currentDate = this.get('currentDate');
                        var date = moment(currentDate);
                        this.setDates(date.add(1,'M'));
                    }
                });
            },
            setDates: function(date) {
                var date = moment(date);
                var currentDate = date.format();
                var dayOfWeek = date.day();
                date.subtract(dayOfWeek,'days')

                var dates = [];
                var x =0;
                while( x < 7) {
                    if(!x) {
                       dates.push(date.format()); 
                    } else {
                        dates.push(date.add(1,'d').format());
                    }
                    x++;
                }
                this.set({dates: dates, currentDate: currentDate});
            },
        });
        return component;
    }
    return daypickerComponent;

})();
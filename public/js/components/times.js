module.exports = (function() {
    'use strict';
    
    var fs = require('fs');
    var moment = require('moment')
    var Ractive = require('ractive');
    var template = fs.readFileSync(__dirname + '/../views/times.html');
    var timeFormComponent = require('./timeform');
    var timeListComponent = require('./timelist');
    var donutChartComponent = require('./donutchart');
    var Immutable = require('immutable');
    var $ = require('jquery');
    
    function timeComponent(actions) {
        var component = Ractive.extend({
            template: template.toString(),
            data: { 
                open: false,
                edit: false,
                form: true,
                fixed: false,
            },
            components: {
                TimeForm: timeFormComponent(actions),
                TimeList: timeListComponent(actions),
                DonutChart: donutChartComponent(actions)
            },
            onrender: function() {

                var formEl = this.find('#fixedColumn');
                var top = formEl.getBoundingClientRect().y;
                var self = this;
                $(window).scroll(function (event) {
                    var y = event.currentTarget.pageYOffset;
                    toggleFixedForm(y,top);
                });
                var toggleFixedForm = function(y,topOffset) {
                    if (y >= topOffset) {
                        // if so, ad the fixed class
                        if(self.get('fixed') !== true) {
                            self.set('fixed', true);
                        }
                    } else {
                        // otherwise remove it
                        if(self.get('fixed') !== false) {
                            self.set('fixed', false);
                        }
                    }
                };
            },
            oninit: function() {
                this.on({
                    
                    toggleColumn : function(event){
                        this.toggle('open');
                        if(this.get('edit')) {
                            this.set('edit',false)
                        }
                    },
                    toggleForm: function(event) {
                        if(event.node.dataset.type === 'form') {
                            this.set({form: true});
                        } else {
                            this.set({form: false});
                        }
                    },
                })
            },
            
        });
        return component;
    }
    return timeComponent;
})();

module.exports = (function() {
    'use strict';
    
    var async = require('../lib/async');
    var moment = require('moment');
    var fs = require('fs');
    var Ractive = require('ractive');
    var $ = require('jquery');
    
    // pick what tests you need 
    require('browsernizr/test/inputtypes');
       
// or if you need access to the modernizr instance: 
    var Modernizr = require('browsernizr');
    
//    var projectDropdownComponent = require('./subprojectdropdown');
    var template = fs.readFileSync(__dirname + '/../views/subprojectform.html');
    
    function subprojectFormComponent(actions) {
        var component = Ractive.extend({
            template: template.toString(),
            data: { 
                subproject: null,
                new_subproject:null,
                html5: false,
                formatDate: function(date) {
                    return moment(date).format('Do MMM YYYY');
                },
            },
            twoway:false,
            isolated:true,
            lazy:true,
            events: {
                tap: require( 'ractive-events-tap' )
            },
            oninit: function() {
                this.initData();
                this.on({
                    
                    cancelForm: function() {
                        this.clearForm();
                    },
                    handleInput: function(event) {
                        this.set('subproject.'+event.original.target.name,event.node.value);
    //                    return false;
                    },
                    submitProject: function(event) {
                        var subproject = this.get('subproject');
                        subproject.project_id = this.get('selected_project')
//                        console.log(subproject);return;
                        if(!subproject) { return };
                        var updateFlag = this.get('edit');
                        if(updateFlag) {
                            actions.subprojects.updateItem(subproject);
                            this.clearForm();
                        } else {
                            actions.subprojects.addItem(subproject);
                            this.clearForm();
                        }
                    },
                    deleteProject: function(event) {
                        if( confirm("Are you sure you wish to delete this subproject?")) {
                            var subproject = this.get('subproject')
                            if(!subproject) { return };
                            actions.subprojects.deleteItem(subproject.id);
                            this.clearForm();
                        }
                    },
                })
            },
            clearForm: function() {
                this.initData();
    //            this.findComponent('DayPicker').set('reset', true);
    //            var subprojectSelect = this.findComponent('ProjectSelect');
    //            subprojectSelect.set('subproject_id', subprojectSelect.get('default_id'));
    //            this.set('subproject.subproject_id',  subprojectSelect.get('default_id'))
                this.set({open:false,edit:false});
            },
            initData: function() {
                var subproject = {};
                subproject.id=0;
                subproject.name = null;
                subproject.description = null;
                this.set({subproject: subproject});
            },
        
        });
        
        return component;
    }
    return subprojectFormComponent;
})();
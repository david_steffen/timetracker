module.exports = (function() {
    'use strict';
    
    var async = require('../lib/async');
    var moment = require('moment');
    var fs = require('fs');
    var Ractive = require('ractive');
    var $ = require('jquery');
    
    // pick what tests you need 
    require('browsernizr/test/inputtypes');
       
// or if you need access to the modernizr instance: 
    var Modernizr = require('browsernizr');
    var template = fs.readFileSync(__dirname + '/../views/projectform.html');
    
    function projectFormComponent(actions) {
        var component = Ractive.extend({
            template: template.toString(),
            data: { 
                project: null,
                new_project:null,
                html5: false,
                formatDate: function(date) {
                    return moment(date).format('Do MMM YYYY');
                },
            },
            twoway:false,
            isolated:true,
            lazy:true,
            components: {
    //            DayPicker: require('rvc!ui/daypicker'),
    //            ProjectDropdown : require('rvc!ui/projectdropdown'),
            },
            events: {
                tap: require( 'ractive-events-tap' )
            },
            oninit: function() {
                this.initData();
                this.isHtml5();
                this.on({
                    
                    cancelForm: function() {
                        this.clearForm();
                    },
                    handleInput: function(event) {
    //                    console.log(event)

                        this.set('project.'+event.original.target.name,event.node.value);
    //                    return false;
                    },
                    submitProject: function(event) {
                        var project = this.get('project');
    //                    console.log(project);return;
                        if(!project) { return };
                        var updateFlag = this.get('edit');
                        if(updateFlag) {
                            actions.projects.updateItem(project);
                            this.clearForm();
                        } else {
                            actions.projects.addItem(project);
                            this.clearForm();
                        }
                    },
                    deleteProject: function(event) {
                        if( confirm("Are you sure you wish to delete this project?")) {
                            var project = this.get('project')
                            if(!project) { return };
                            actions.projects.deleteItem(project.id);
                            this.clearForm();
                        }
                    },
                })
            },
            decorators: {
                colorpicker: require('./colorpickerdecorator')
            },
            isHtml5: function() {
                this.set('html5',Modernizr.inputtypes.color);
            },
            clearForm: function() {
                this.initData();
    //            this.findComponent('DayPicker').set('reset', true);
    //            var projectSelect = this.findComponent('ProjectSelect');
    //            projectSelect.set('project_id', projectSelect.get('default_id'));
    //            this.set('project.project_id',  projectSelect.get('default_id'))
                this.set({open:false,edit:false});
            },
            initData: function() {
                var project = {};
                project.id=0;
                project.colour = '#53868B';
                project.name = null;
                project.abbreviation = null;
                project.company = null;
                this.set({project: project});
            },
        
        });
        return component;
    }
    return projectFormComponent;
})();
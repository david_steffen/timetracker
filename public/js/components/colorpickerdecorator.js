module.exports = (function() {
    'use strict';
    
    var $ = require('jquery');
    var minicolors = require('jquery-minicolors');
    return function(node,colour){
        $(node).val(colour)
       $(node).minicolors({theme: 'custom',color: colour});
        return {
            update: function() {
                console.log(colour)
              $(node).minicolors({theme: 'custom',color: colour}); 
            },
            teardown: function () {
                $(node).minicolors('destroy');
            }
        };
    };
})();

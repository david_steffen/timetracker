module.exports = (function() {
    'use strict';
    
    var fs = require('fs');
//    var async = require('async');
    var moment = require('moment');
    var Ractive = require('ractive');
    var template = fs.readFileSync(__dirname + '/../views/projects.html');
    var Immutable = require('immutable');
    var $ = require('jquery');
    
    function projectComponent(actions) {
        var component = Ractive.extend({
            template: template.toString(),
            data: {
                open: false,
                edit: false,
                fixed: false,
                form: true,
                formatDate: function(date) {
                    return moment(date).format('ddd Do MMM YYYY');
                },
            }, 
            onrender: function() {
                var formEl = this.find('#fixedColumn');
                var top = formEl.getBoundingClientRect().y;
                var self = this;
                $(window).scroll(function (event) {
                    var y = event.currentTarget.pageYOffset;
                    toggleFixedForm(y,top);
                });
                var toggleFixedForm = function(y,topOffset) {
                    if (y >= topOffset) {
                        // if so, ad the fixed class
                        self.set('fixed', true);
                    } else {
                        // otherwise remove it
                        self.set('fixed', false);
                    }
                };
            },
            oninit: function() {
    //            this.root.observe('projects',function(one,two,three)
    //            {
    //                console.log(one,two,three)
    //            })
                this.on({
                    editProject: function(event) {
                        var project = Immutable.Map(this.get(event.keypath));
                        var new_project = project.toJS();
//                        console.log(new_project)
                        this.findComponent('ProjectForm').set('project',new_project);
                        this.set({edit: true,open: true});
//                        var projectDropdown = this.findComponent('ProjectDropdown');
//                        projectDropdown.set('item_id',this.get(time).project_id);
//                        projectDropdown.set('subproject_id',this.get(time).subproject_id);
//                        this.findComponent('DayPicker').set('newDate',this.get(time).log_date);
                    },
                    toggleColumn : function(event){
                        this.toggle('open');
                        if(this.get('edit')) {
                            this.set('edit',false)
                        }
                    },
                    toggleForm: function(event) {
                        if(event.node.dataset.type === 'form') {
                            this.set({form: true});
                        } else {
                            this.set({form: false});
                        }
                    },
                })
            },
        });
        return component;
    }
    return projectComponent;
    
});

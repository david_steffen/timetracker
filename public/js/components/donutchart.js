module.exports = (function() {
    'use strict';
    
    
    var fs = require('fs');
    var Ractive = require('ractive');
    var template = fs.readFileSync(__dirname + '/../views/donutchart.html');

    function donutChartComponent(actions) {
        var component = Ractive.extend({
            template: template.toString(),
            oninit: function () {
                var self = this, delay = this.get( 'delay' );
                
                // wait a bit, then animate in
                setTimeout( function () {
                    self.animate( 'c', Math.PI * 2, {
                        duration: 800,
                        easing: 'easeOut'
                    });
                }, delay );
            },

            data: {
                delay: 50,
                c: 0, // we animate from zero to Math.PI * 2 (the number of radians in a circle)
                innerRadius: 20,
                outerRadius: 45,
                selected: null,
                plot: function ( segment ) {
                    var innerRadius = this.get( 'innerRadius' );
                    var outerRadius = this.get( 'outerRadius' );
                    var c = this.get( 'c' ); // allows us to animate intro

                    var start = segment.start * c;
                    var end = segment.end * c;

                    function getPoint ( angle, radius ) {
                        return ( ( radius * Math.sin( angle ) ).toFixed( 2 ) + ',' + ( radius * -Math.cos( angle ) ).toFixed( 2 ) );
                    }

                    var points = [];
                    var angle;

                    // get points along the outer edge of the segment
                    for ( angle = start; angle < end; angle += 0.05 ) {
                        points.push( getPoint( angle, outerRadius ) );
                    }

                    points.push( getPoint( end, outerRadius ) );

                    // get points along the inner edge of the segment
                    for ( angle = end; angle > start; angle -= 0.05 ) {
                        points.push( getPoint( angle, innerRadius ) );
                    }

                    points.push( getPoint( start, innerRadius ) );

                    // join them up as an SVG points list
                    return points.join( ' ' );
                }
            },

            computed: {
                segments: function () {
                    var points = this.get( 'points' );
                    
                    var keys = Object.keys( points ).sort( function ( a, b ) {
                        return points[b] - points[a];
                    });

                    // tally up the total value
                    var total = keys.reduce( function ( total, id ) {
                        return total + points[id];
                    }, 0 );

                    // find the start and end point of each segment
                    var start = 0;

                    return keys.map( function ( id ) {
                        var size = points[id] / total, end = start + size, segment;

                        segment = {
                            id: id,
                            start: start,
                            end: end
                        };

                        start = end;
                        return segment;
                    });
                }
            },
        });
        return component;
    }
    return donutChartComponent;
})();
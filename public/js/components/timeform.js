module.exports = (function() {
    'use strict';
    
    var fs = require('fs');
    var Ractive = require('ractive');
    var moment = require('moment');
    var template = fs.readFileSync(__dirname + '/../views/timeform.html');
    var daypickerComponent = require('./daypicker');
    var async = require('../lib/async');
    
    function timeFormComponent(actions) {
        var component = Ractive.extend({
            template: template.toString(),
            data: { 
                time: null,
                formatDate: function(date) {
                    return moment(date).format('Do MMM YYYY');
                },
            },
            components: {
                DayPicker: daypickerComponent(actions),
            },
            oninit: function() {         
                this.initData();
                this.on({
                    
                    cancelForm: function() {
                        this.clearForm();
                    },
                    handleInput: function(event) {
                        this.set('time.'+event.original.target.name,event.original.target.value);
                    },
                    submitTime: function(event) {
                        var time = this.get('time')
                        var projectDropdown = this.findComponent('ProjectDropdown');
                        time.project_id = this.get('projectStore.selected_project');
                        time.subproject_id= projectDropdown.get('subproject_id');
                        time.log_date = moment(this.findComponent('DayPicker').get('currentDate')).format('YYYY-MM-DD HH:mm:ss');
                        if(!time) { return };
                        var updateFlag = this.get('edit');
                        if(updateFlag) {
                            
                            actions.times.updateItem(time);
                            this.clearForm();
                        } else {
                            actions.times.addItem(time);
                            this.clearForm();
                        }
                    },
                    deleteTime: function(event) {
                        if( confirm("Are you sure you wish to delete this time?")) {
                            var time = this.get('time');
                            if(!time) { return };
                            actions.times.deleteItem(time.id);
                            this.clearForm();
                        }
                    },
                })
            },
            
            clearForm: function() {
                this.initData();
    //            this.findComponent('DayPicker').set('reset', true);
    //            var projectSelect = this.findComponent('ProjectSelect');
    //            projectSelect.set('project_id', projectSelect.get('default_id'));
    //            this.set('time.project_id',  projectSelect.get('default_id'))
                this.set({open:false,edit:false});
            },
            initData: function() {
                var time = {};
                time.id = null;
                time.log_date = moment().format();
                time.time = null;
                time.description = null;
//                time.project_id = this.findComponent('ProjectDropdown').get('default_id');
                this.set('time', time);
            },
        });
        return component;
    }
        return timeFormComponent;
})();
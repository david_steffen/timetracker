module.exports = (function() {
    'use strict';
    
    var fs = require('fs');
    var Ractive = require('ractive');
    var template = fs.readFileSync(__dirname + '/../views/projectdropdown.html');
    var $ = require('jquery');
    
    function projectDropdownComponent(actions) {
        var component = Ractive.extend({
            template: template.toString(),
            data: {
                active: false,
                item_id:  0,
                two_level: true,
        //        default_id: 0,
                subproject_id: 0,
            },
            oninit: function() {
                this.on({
                    toggleSelect: function(event) {
                        this.toggle('active');
                    },
                    selectProject: function(event) {
                        actions.projects.selectProject(event.context.id)
                        this.set({active:false,item_id:event.context.id,subproject_id:0});
                    },
                    selectSubproject: function(event){
                        var split = event.keypath.split('.');
                         actions.projects.selectProject(split[1])
                        this.set({active:false,item_id:split[1],subproject_id:split[3]});

                    }
                });
            },
            
        });
        return component;
    }
    return projectDropdownComponent;
})();
    
module.exports = (function() {
    'use strict';
    
    var fs = require('fs');
    var moment = require('moment');
    var Ractive = require('ractive');
    var template = fs.readFileSync(__dirname + '/../views/timer.html');
    function timerComponent() {
        var component = Ractive.extend({
            template: template.toString(),
            data: {
                timer: 0,
                init: false,
            },
            oninit: function () {
                var self = this, interval;
                this.on({
                    handleTimer: function() {
                        if(this.get('init')) { 
                            clearInterval( interval );
                            this.set('init',false);
                            return;
                        }
                        interval = setInterval(function(){
                            time = self.get('timer') + 1;
                            self.set('timer',time);
                        },1000);
                        this.set('init',true);
                        
                    },
                    teardown: function () {
                        clearInterval( interval );
                    }
                });
            },
        });
        return component;
    }
    return timerComponent;

})();
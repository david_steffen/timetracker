module.exports = (function() {
    'use strict';
    var async = require('../lib/async');
    var fs = require('fs');
    var page = require('page');
    var Ractive = require('ractive');
    var keys = require( 'ractive-events-keys' );
    var template = fs.readFileSync(__dirname + '/../views/login.html');
    function loginComponent() {
        var component = Ractive.extend({
            template: template.toString(),
            data: {
                email: null,
                password: null
            },
            events: {
                enter: keys.enter,
            },
            oninit: function () {
                this.on({
                    handleEmail: function(event) {
                        this.set('email',event.original.target.value);
                    },
                    handlePassword: function(event) {
                        this.set('password',event.original.target.value);
                    },

                    login: function(event) {
                        event.original.preventDefault();
                        var self = this;
                        var data = {email: this.get('email'),password: this.get('password')};
                        async('post','login',data)
                            .done(function(resp) {
                                localStorage.setItem("token", resp.data.token);
                                localStorage.setItem("expiry_date", resp.data.expiry_date);
                                self.parent.set('signedIn', true);
                                page.redirect('times')
                            }).fail(function(resp){
                    //            self.setState({error: true});
                    //            self.setState({err_msg: _data.responseJSON.payload.err_msg});
                            });
                    },
                })

            },
        });
        return component;
    }
    return loginComponent;

})();
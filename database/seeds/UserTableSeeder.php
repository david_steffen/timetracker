<?php
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UserTableSeeder extends Seeder {
 
    public function run()
    {
        DB::table('users')->delete();
 
        User::create(array(
            'first_name'=>'dave',
            'last_name'=>'moo',
            'email' => 'david@email.com',
            'password' => Hash::make('1234567'),
            'account_id' => 1,
        ));
 
        User::create(array(
            'first_name'=>'dave',
            'last_name'=>'moo',
            'email' => 'dave@email.com',
            'password' => Hash::make('1234567'),
            'account_id' => 1,
        ));
    }
 
}

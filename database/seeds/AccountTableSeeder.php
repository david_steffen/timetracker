<?php
use App\Account;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class AccountTableSeeder extends Seeder {
 
    public function run()
    {
        DB::table('accounts')->delete();
 
        Account::create(array(
            'company' => 'test',
        ));
    }
 
}


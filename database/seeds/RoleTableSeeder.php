<?php
use App\Role;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class RoleTableSeeder extends Seeder {
 
    public function run()
    {
        DB::table('roles')->delete();
 
        Role::create(array(
            'name' => 'single_user',
            'display_name' => 'Single user account',
            'description' => 'blah',
        ));
 
        Role::create(array(
            'name' => 'admin',
            'display_name' => 'Admin user account',
            'description' => 'blah',
        ));
    }
 
}


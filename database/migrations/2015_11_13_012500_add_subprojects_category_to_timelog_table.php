<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubprojectsCategoryToTimelogTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('time_logs', function(Blueprint $table)
		{
                    $table->integer('subproject_id')->unsigned();
//                    $table->foreign('subproject_id')->references('id')->on('subprojects');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('time_logs', function(Blueprint $table)
		{
                    $table->dropColumn('subproject_id');
		});
	}

}

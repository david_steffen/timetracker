<?php namespace App\Transformers;

use League\Fractal\TransformerAbstract;

class AuthTransformer extends TransformerAbstract {


    public function transform($data)
    {
        return [
            'token'            => $data->token,
            'expiry_date'       => $data->expiry
        ];
    }



}


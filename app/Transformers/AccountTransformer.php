<?php namespace App\Transformers;

use App\Account;  
use League\Fractal\TransformerAbstract;

class AccountTransformer extends TransformerAbstract {

//    protected $defaultIncludes = [
//        'notes'
//    ];

    public function transform(Account $account)
    {
        return [
            
            'id'            => $account->id,
            'company'       => $account->company,
            'telephone'     => $account->telephone,
            'updated_at'    => $account->updated_at->format('F d, Y')
        ];
    }

//    public function includeNotes(Project $project)
//    {
//        $notes = $project->notes;
//
//        return $this->collection($notes, new NoteTransformer);
//    }

}
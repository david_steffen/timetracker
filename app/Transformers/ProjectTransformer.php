<?php namespace App\Transformers;

use App\Project;  
use League\Fractal\TransformerAbstract;

class ProjectTransformer extends TransformerAbstract {

    protected $defaultIncludes = [
        'subprojects'
    ];

    public function transform(Project $project)
    {
        return [
            'id'            => $project->id,
            'name'          => $project->name,
            'colour'        => $project->colour,
            'abbreviation'  => $project->abbreviation,
            'company'       => $project->company,
            'time_this_week'=> $project->time_this_week(),
            'total_time'    => $project->total_time(),
            'updated_at'    => $project->updated_at->format('F d, Y')
        ];
    }

    public function includeSubprojects(Project $project)
    {
        $subprojects = $project->subprojects;

        return $this->collection($subprojects, new SubprojectTransformer);
    }

}
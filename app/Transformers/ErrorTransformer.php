<?php namespace App\Transformers;

  
use League\Fractal\TransformerAbstract;

class ErrorTransformer extends TransformerAbstract {

//    protected $defaultIncludes = [
//        'notes'
//    ];

    public function transform($error)
    {
        return [
            'error'        => $error,

        ];
    }

//    public function includeNotes(Project $project)
//    {
//        $notes = $project->notes;
//
//        return $this->collection($notes, new NoteTransformer);
//    }

}
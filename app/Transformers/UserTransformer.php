<?php namespace App\Transformers;

use App\User;  
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract {

    protected $defaultIncludes = [
        'roles',
        'account'
    ];

    public function transform(User $user)
    {
        return [
            
            'id'            => $user->id,
            'name'          => $user->name,
            'email'         => $user->email,
            'updated_at'    => $user->updated_at->format('F d, Y')
        ];
    }

    public function includeRoles(User $user)
    {
        $roles = $user->roles;

        return $this->collection($roles, new RoleTransformer);
    }
    
    public function includeAccount(User $user)
    {
        $account = $user->account;

        return $this->item($account, new AccountTransformer);
    }
}
<?php namespace App\Transformers;

use App\Subproject;  
use League\Fractal\TransformerAbstract;

class SubprojectTransformer extends TransformerAbstract {

//    protected $defaultIncludes = [
//        'subprojects'
//    ];

    public function transform(Subproject $subproject)
    {
        return [
            'id'            => $subproject->id,
            'name'          => $subproject->name,
            'description'   => $subproject->description,
            'updated_at'    => $subproject->updated_at->format('F d, Y')
        ];
    }

//    public function includeSubprojects(Project $project)
//    {
//        $subprojects = $project->subprojects;
//
//        return $this->collection($subprojects, new SubprojectTransformer);
//    }

}
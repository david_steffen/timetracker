<?php namespace App\Transformers;

use App\TimeLog;  
use League\Fractal\TransformerAbstract;

class TimeLogTransformer extends TransformerAbstract {

//    protected $defaultIncludes = [
//        'notes'
//    ];

    public function transform(TimeLog $timelog)
    {
        return [
            'id'            => $timelog->id,
            'description'   => $timelog->description,
            'time'          => $timelog->time,
            'log_date'      => $timelog->log_date,
            'project_id'    => $timelog->project_id,
            'subproject_id' => $timelog->subproject_id,
            'updated_at'    => $timelog->updated_at->format('F d, Y')
        ];
    }

//    public function includeNotes(Project $project)
//    {
//        $notes = $project->notes;
//
//        return $this->collection($notes, new NoteTransformer);
//    }

}
<?php namespace App\Transformers;

  
use League\Fractal\TransformerAbstract;
use Illuminate\Validation\Validator;

class ValidationTransformer extends TransformerAbstract {

//    protected $defaultIncludes = [
//        'notes'
//    ];

    public function transform(Validator $validation)
    {
        return [
            'error'        => $validation->errors(),

        ];
    }

//    public function includeNotes(Project $project)
//    {
//        $notes = $project->notes;
//
//        return $this->collection($notes, new NoteTransformer);
//    }

}
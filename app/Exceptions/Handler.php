<?php namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Transformers\ErrorTransformer;
use League\Fractal\Manager; 
use League\Fractal\Resource\Item;
use Illuminate\Http\Response as IlluminateResponse;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Tymon\JWTAuth\Exceptions\JWTException;

class Handler extends ExceptionHandler {

	/**
	 * A list of the exception types that should not be reported.
	 *
	 * @var array
	 */
	protected $dontReport = [
		'Symfony\Component\HttpKernel\Exception\HttpException',
        'Illuminate\Database\Eloquent\ModelNotFoundException;'
	];

	/**
	 * Report or log an exception.
	 *
	 * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
	 *
	 * @param  \Exception  $e
	 * @return void
	 */
	public function report(Exception $e)
	{
		return parent::report($e);
	}

	/**
	 * Render an exception into an HTTP response.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Exception  $e
	 * @return \Illuminate\Http\Response
	 */
	public function render($request, Exception $e)
	{
		switch($e){

            case ($e instanceof ModelNotFoundException):
            case ($e instanceof HttpException):
            case ($e instanceof NotFoundHttpException):
            case ($e instanceof TokenExpiredException):
            case ($e instanceof TokenInvalidException):
            case ($e instanceof JWTException):
                return $this->renderException($e);
                break;
            default:

                return parent::render($request, $e);

        }
	}
    
    protected function renderException($e)
    {
        switch ($e){

            case ($e instanceof ModelNotFoundException):
                $error = 'No results found';
                $errorTransformer = new ErrorTransformer();
                $item = new Item($error, $errorTransformer);
                $fractal = new Manager();
                $data = $fractal->createData($item)->toArray();
                return response()->json($data, IlluminateResponse::HTTP_NOT_FOUND, []);
                break;
            case ($e instanceof HttpException):
            case ($e instanceof TokenExpiredException):
            case ($e instanceof TokenInvalidException):
                $error = $e->getMessage();
                $errorTransformer = new ErrorTransformer();
                $item = new Item($error, $errorTransformer);
                $fractal = new Manager();
                $data = $fractal->createData($item)->toArray();
                return response()->json($data, $e->getStatusCode(), []);
                break;
            case ($e instanceof NotFoundHttpException):
                $error = 'Sorry, the resource was not found';
                $errorTransformer = new ErrorTransformer();
                $item = new Item($error, $errorTransformer);
                $fractal = new Manager();
                $data = $fractal->createData($item)->toArray();
                return response()->json($data, $e->getStatusCode(), []);
                break;
            case ($e instanceof JWTException):
                $error = 'Failed to create token';
                $errorTransformer = new ErrorTransformer();
                $item = new Item($error, $errorTransformer);
                $fractal = new Manager();
                $data = $fractal->createData($item)->toArray();
                return response()->json($data, IlluminateResponse::HTTP_INTERNAL_SERVER_ERROR, []);
                break;
            default:
                return (new SymfonyDisplayer(config('app.debug')))
                       ->createResponse($e);

        }
    }

}

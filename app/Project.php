<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model {

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'projects';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['colour', 'abbreviation', 'name','company','status_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];

    public function members() 
    {
        return $this->belongsToMany('App\User', 'project_members');
    }
    public function subprojects()
    {
        return $this->hasMany('App\Subproject','project_id');
    }
    public function time_logs()
    {
        return $this->hasMany('App\TimeLog','project_id');
    }
    public function time_this_week() 
    {
        $start = date('w');
        $date = new \DateTime();
        $prev_date = $date->sub(new \DateInterval('P'.$start.'D'))->format("Y-m-d 00:00:00");
        $date = new \DateTime();
        $now = $date->format("Y-m-d H:i:s");
        return $this->time_logs()->whereBetween('log_date', [ $prev_date, $now])->sum('time');
    }

    public function total_time()
    {
        return $this->time_logs()->sum('time');
    }
}

<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Zizaco\Entrust\Traits\EntrustUserTrait;

// use App\Exceptions\InsufficientPermissionException;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword, EntrustUserTrait, SoftDeletes;

    protected $dates = ['deleted_at'];
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password', 'acount_id'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];
        
    public function time_logs() {
        return $this->hasMany('App\TimeLog');
    }
    public function projects() {
        return $this->belongsToMany('App\Project', 'project_members');
    }
    public function account() {
        return $this->belongsTo('App\Account');
    }
    public function hasRolesOrFail($allowed_roles)
    {
        if($this->hasRole($allowed_roles))
        {
            return true;
        }
        else 
        {
            abort(403,'You are not authorized to access this resource');
            
        }
    }
}

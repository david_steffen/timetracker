<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectMember extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'project_members';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'project_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
//    protected $hidden = ['created_at', 'updated_at'];
    public function project()
    {
        return $this->hasOne('App\Project','id','project_id');
    }
     
    public function member()
    {
        return $this->hasOne('App\User');
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'ClientController@index');
Route::get('/times', 'ClientController@index');
Route::get('/projects', 'ClientController@index');
Route::get('/logout', 'ClientController@index');
Route::get('/login', 'ClientController@index');


Route::post('api/login', 'Auth\AuthController@login');


App::bind('League\Fractal\Serializer\SerializerAbstract', 'League\Fractal\Serializer\DataArraySerializer'); 
// Route group for API versioning
Route::group(['prefix' => 'api','middleware' => 'jwt.auth'], function() {
    Route::get('user', 'Auth\AuthController@getActiveUser');
    Route::resource('times', 'TimeLogController');
    Route::resource('projects', 'ProjectController');
    Route::resource('subprojects', 'SubprojectController');
    Route::resource('account', 'AccountController');
    Route::resource('users', 'UserController');
    Route::resource('account-users', 'AccountUserController');
    Route::resource('project-members', 'AccountUserController');
});




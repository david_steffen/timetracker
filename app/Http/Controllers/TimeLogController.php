<?php namespace App\Http\Controllers;

use Illuminate\Http\Response as IlluminateResponse;
use App\Http\Controllers\ApiController;
use Auth;
use App\TimeLog;
use Illuminate\Support\Facades\Validator;
use App\Transformers\TimeLogTransformer;
use App\Transformers\ValidationTransformer;
use App\Transformers\ErrorTransformer; 
use Illuminate\Http\Request;  
use League\Fractal\Manager;  
use League\Fractal\Resource\Collection;  
use League\Fractal\Resource\Item;

class TimeLogController extends ApiController {

    protected $fractal;
    protected $timelogTransformer;
    protected $validationTransformer;
    protected $errorTransformer;
    
    public function __construct(Manager $fractal, TimeLogTransformer $timelogTransformer,ValidationTransformer $validationTransformer, ErrorTransformer $errorTransformer)
    {
        $this->fractal = $fractal;
        $this->timelogTransformer = $timelogTransformer;
        $this->validationTransformer = $validationTransformer;
        $this->errorTransformer = $errorTransformer;
    }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $times = TimeLog::where('user_id','=',Auth::user()->id)->orderBy('log_date')->get();
        if(!count($times)) {
            $this->setStatusCode(IlluminateResponse::HTTP_NOT_FOUND);
        }
        $collection = new Collection($times, $this->timelogTransformer);

        $data = $this->fractal->createData($collection)->toArray();

        return $this->respond($data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        // validate
        $validate = Validator::make($request->all(), [
            'time' => 'required',
//            'description' => 'required',
            'project_id' => 'required'
        ]);
        if ($validate->fails())
        {

            $this->setStatusCode(IlluminateResponse::HTTP_BAD_REQUEST);
            $item = new Item($validate, $this->validationTransformer);
        } 
        else 
        {
            $time = new TimeLog;
            $time->time = $request->input('time');
            $time->description = $request->input('description');
            $time->user_id = Auth::user()->id;
            $time->project_id = $request->input('project_id');
            $time->subproject_id = $request->input('subproject_id',null);
            $time->log_date = $request->input('log_date', date('Y-m-d H:i:s'));
            if ($time->save())
            {
                $this->setStatusCode(IlluminateResponse::HTTP_CREATED);
                $item = new Item($time, $this->timelogTransformer);
            } else {
                $this->setStatusCode(IlluminateResponse::HTTP_INTERNAL_SERVER_ERROR);
                $error = "The time log was not saved";
                $item = new Item($error, $this->errorTransformer);
            }


        }

        $data = $this->fractal->createData($item)->toArray();
        return $this->respond($data);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $time = TimeLog::where('id','=',$id)->where('user_id','=',Auth::user()->id)->firstOrFail();

        $item = new Item($time, $this->timelogTransformer);

        $data = $this->fractal->createData($item)->toArray();

        return $this->respond($data);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
    public function update($id, Request $request)
	{
        // validate
        $validate = Validator::make($request->all(), [
            'id' => 'required',
            'time' => 'required',
//            'description' => 'required',
            'project_id' => 'required'
        ]);
        if ($validate->fails())
        {

            $this->setStatusCode(IlluminateResponse::HTTP_BAD_REQUEST);
            $item = new Item($validate, $this->validationTransformer);
        }
        else 
        {
            $time = TimeLog::where('id','=',$request->input('id'))->where('user_id','=',Auth::user()->id)->firstOrFail();
            $time->time = $request->input('time');
            $time->description = $request->input('description');
            $time->project_id = $request->input('project_id');
            $time->subproject_id = $request->input('subproject_id',null);
            $time->log_date = $request->input('log_date', date('Y-m-d H:i:s'));
            if ($time->save())
            {
                $this->setStatusCode(IlluminateResponse::HTTP_CREATED);
                $item = new Item($time, $this->timelogTransformer);
            } else {
                $this->setStatusCode(IlluminateResponse::HTTP_INTERNAL_SERVER_ERROR);
                $error = "The time log was not saved";
                $item = new Item($error, $this->errorTransformer);
            }

        }

        $data = $this->fractal->createData($item)->toArray();
        return $this->respond($data);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $time = TimeLog::where('id','=',$id)->where('user_id','=',Auth::user()->id)->firstOrFail();
        if (!$time->delete())
        {
            $this->setStatusCode(IlluminateResponse::HTTP_INTERNAL_SERVER_ERROR);
            $error = "The time log was not deleted";
            $item = new Item($error, $this->errorTransformer);
            $data = $this->fractal->createData($item)->toArray();
            return $this->respond($data);
        }
            
	}

}

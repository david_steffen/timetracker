<?php namespace App\Http\Controllers;

use Illuminate\Http\Response as IlluminateResponse;
use App\Http\Controllers\ApiController;
use Auth;
use App\Account;
use App\Transformers\UserTransformer;
use App\Transformers\ValidationTransformer;
use App\Transformers\ErrorTransformer;
use Illuminate\Http\Request;  
use League\Fractal\Manager;  
use League\Fractal\Resource\Collection;  
use League\Fractal\Resource\Item;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Config;

class AccountUserController extends ApiController {
    
    protected $fractal;
    protected $userTransformer;
    protected $validationTransformer;
    protected $errorTransformer;


    public function __construct(Manager $fractal, UserTransformer $userTransformer, 
                ValidationTransformer $validationTransformer, ErrorTransformer $errorTransformer) {
        $this->fractal = $fractal;
        $this->userTransformer = $userTransformer;
        $this->validationTransformer = $validationTransformer;
        $this->errorTransformer = $errorTransformer;
    }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $user = Auth::user();
        $user->hasRolesOrFail(Config::get('app.route_permissions.account_users'));
        
		$account_id = $user->account->id;
        $users = Account::findOrFail($account_id)->users;
        $collection = new Collection($users, $this->userTransformer);

        $data = $this->fractal->createData($collection)->toArray();

        return $this->respond($data);
        
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}

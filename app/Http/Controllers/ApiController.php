<?php namespace App\Http\Controllers;

use Illuminate\Http\Response as IlluminateResponse;
use Auth;
use InsufficientPermissionException;

class ApiController extends Controller {

    protected $statusCode = IlluminateResponse::HTTP_OK;

    public function getStatusCode()
    {
        return $this->statusCode;
    }

    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    public function respond($data, $headers = [])
    {
        return response()->json($data, $this->getStatusCode(), $headers);
    }
    
    
}

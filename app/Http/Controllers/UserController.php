<?php namespace App\Http\Controllers;

use Illuminate\Http\Response as IlluminateResponse;
use App\Http\Controllers\ApiController;
use Auth;
use App\User;
use App\Account;
use App\Transformers\UserTransformer;
use App\Transformers\ValidationTransformer;
use App\Transformers\ErrorTransformer; 
use Illuminate\Http\Request;  
use League\Fractal\Manager;  
use League\Fractal\Resource\Collection;  
use League\Fractal\Resource\Item;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Config;

class UserController extends ApiController {
    protected $fractal;
    protected $userTransformer;
    protected $validationTransformer;
    protected $errorTransformer;


    public function __construct(Manager $fractal, UserTransformer $userTransformer, 
                ValidationTransformer $validationTransformer, ErrorTransformer $errorTransformer) {
        $this->fractal = $fractal;
        $this->userTransformer = $userTransformer;
        $this->validationTransformer = $validationTransformer;
        $this->errorTransformer = $errorTransformer;
    }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$user = Auth::user();
       
        $user->hasRolesOrFail(Config::get('app.route_permissions.users'));
        
        $users = Account::findOrFail($user->account->id)->users;

        $collection = new Collection($users, $this->userTransformer);

        $data = $this->fractal->createData($collection)->toArray();

        return $this->respond($data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$user = Auth::user();
              
        $user->hasRolesOrFail(Config::get('app.route_permissions.users'));
        
        if($id !== $user->id) {
            
            $user = User::where('id','=',$id)->where('account_id','=',$user->account->id)->firstOrFail();
        }

        $item = new Item($user, $this->userTransformer);

        $data = $this->fractal->createData($item)->toArray();

        return $this->respond($data);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id,Request $request)
	{
        $user = Auth::user();
        
//        $user->hasRolesOrFail(Config::get('app.route_permissions.projects'));
        // validate
        $validate = Validator::make($request->all(), [
            'name' => 'required',
        ]);
        if ($validate->fails())
        {
            $this->setStatusCode(IlluminateResponse::HTTP_BAD_REQUEST);
            $item = new Item($validate, $this->validationTransformer);
        } 
        else 
        {
            $user->name = $request->input('name');
            if ($user->save())
            {
                $this->setStatusCode(IlluminateResponse::HTTP_CREATED);
                $item = new Item($user, $this->userTransformer);
            } else {
                $this->setStatusCode(IlluminateResponse::HTTP_INTERNAL_SERVER_ERROR);
                $error = "Your details was not updated";
                $item = new Item($error, $this->errorTransformer);
            }

        }
        $data = $this->fractal->createData($item)->toArray();
        return $this->respond($data);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$user = Auth::user();

        if ($user->detach()->delete())
        {
            $this->setStatusCode(IlluminateResponse::HTTP_INTERNAL_SERVER_ERROR);
            $error = "Your profile has been deleted";
            $item = new Item($error, $this->errorTransformer);
            $data = $this->fractal->createData($item)->toArray();
            return $this->respond($data);
        }
	}

}

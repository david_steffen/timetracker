<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Response as IlluminateResponse;
use App\Http\Controllers\ApiController;  
use JWTAuth;
use App\Transformers\AuthTransformer;
use Illuminate\Http\Request;
use App\Transformers\UserTransformer;
use App\Transformers\ValidationTransformer;
use App\Transformers\ErrorTransformer; 
use League\Fractal\Manager;  
use League\Fractal\Resource\Item;
use Illuminate\Support\Facades\Validator;

class AuthController extends ApiController {

    protected $fractal;
    protected $authTransformer;
    protected $validationTransformer;
    protected $errorTransformer;


    public function __construct(Manager $fractal, AuthTransformer $authTransformer, 
                ValidationTransformer $validationTransformer, ErrorTransformer $errorTransformer) {
        $this->fractal = $fractal;
        $this->authTransformer = $authTransformer;
        $this->validationTransformer = $validationTransformer;
        $this->errorTransformer = $errorTransformer;
    }
    /**
     * Handle a login request to the application.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'email' => 'required|email|max:255', 'password' => 'required',
        ]);
        if ($validate->fails())
        {
            $this->setStatusCode(IlluminateResponse::HTTP_BAD_REQUEST);
            $item = new Item($validate, $this->validationTransformer);
        } 
        else 
        {
            $credentials = $this->getCredentials($request);
            $token = JWTAuth::attempt($credentials);
            $payload = JWTAuth::getPayload($token);
            $auth = new \stdClass();
            $auth->token = $token;
            $auth->expiry = $payload['exp'];
            if (!$token)
            {
                $error = 'Invalid credentials';
                $this->setStatusCode(IlluminateResponse::HTTP_UNAUTHORIZED);
                $item = new Item($error, $this->errorTransformer);
            }
            else {
                $this->setStatusCode(IlluminateResponse::HTTP_CREATED);
                $item = new Item($auth, $this->authTransformer);
            }
        }
        
        $data = $this->fractal->createData($item)->toArray();
        return $this->respond($data);
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function getCredentials(Request $request)
    {
        return $request->only('email', 'password');
    }
    
    /**
     * Get logged in user object
     * 
     * @param UserTransformer $userTransformer
     * @return type
     */
    public function getActiveUser(UserTransformer $userTransformer)
    {
        $user = JWTAuth::parseToken()->toUser();
        $item = new Item($user, $userTransformer);

        $data = $this->fractal->createData($item)->toArray();

        return $this->respond($data);
    }
    public function postRegisterUser(UserTransformer $userTransformer)
    {
        $validate = Validator::make($request->all(), [
            'email' => 'required|email|max:255',
            'password' => 'required',
        ]);
        if ($validate->fails())
        {
            $this->setStatusCode(IlluminateResponse::HTTP_BAD_REQUEST);
            $item = new Item($validate, $this->validationTransformer);
        } 
        $user = JWTAuth::parseToken()->toUser();
        $item = new Item($user, $userTransformer);

        $data = $this->fractal->createData($item)->toArray();

        return $this->respond($data);
    }
}

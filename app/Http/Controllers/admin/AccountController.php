<?php namespace App\Http\Controllers;

use Illuminate\Http\Response as IlluminateResponse;
use App\Http\Controllers\ApiController;
use Auth;
use App\Account; 
//use App\ProjectMember;
use App\Transformers\AccountTransformer;
use App\Transformers\ValidationTransformer;
use App\Transformers\ErrorTransformer;
use League\Fractal\Manager;  
use League\Fractal\Resource\Collection;  
use League\Fractal\Resource\Item;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Config;

//use Illuminate\Http\Request;

class AccountController extends ApiController {

    protected $fractal;
    protected $accountTransformer;
    protected $validationTransformer;
    protected $errorTransformer;


    public function __construct(Manager $fractal, AccountTransformer $accountTransformer, 
                ValidationTransformer $validationTransformer, ErrorTransformer $errorTransformer) {
        
        Auth::user()->hasRolesOrFail(Config::get('app.route_permissions.accounts'));
        $this->fractal = $fractal;
        $this->accountTransformer = $accountTransformer;
        $this->validationTransformer = $validationTransformer;
        $this->errorTransformer = $errorTransformer;
    }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		
        
        $accounts = Account::all();

        $collection = new Collection($accounts, $this->accountTransformer);

        $data = $this->fractal->createData($collection)->toArray();

        return $this->respond($data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}

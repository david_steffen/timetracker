<?php namespace App\Http\Controllers;


use Illuminate\Http\Response as IlluminateResponse;
use App\Http\Controllers\ApiController;
use Auth;
use App\Subproject;
use App\ProjectMember;
use App\Transformers\SubprojectTransformer;
use App\Transformers\ValidationTransformer;
use App\Transformers\ErrorTransformer; 
use Illuminate\Http\Request;  
use League\Fractal\Manager;  
use League\Fractal\Resource\Collection;  
use League\Fractal\Resource\Item;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Config;

class SubprojectController extends ApiController {
    protected $fractal;
    protected $subprojectTransformer;
    protected $validationTransformer;
    protected $errorTransformer;


    public function __construct(Manager $fractal, SubprojectTransformer $subprojectTransformer, 
                ValidationTransformer $validationTransformer, ErrorTransformer $errorTransformer) {
        $this->fractal = $fractal;
        $this->subprojectTransformer = $subprojectTransformer;
        $this->validationTransformer = $validationTransformer;
        $this->errorTransformer = $errorTransformer;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
//		$user = Auth::user();
//       
//        $user->hasRolesOrFail(Config::get('app.route_permissions.users'));
//        
//        $subproject = Subproject::findOrFail($user->account->id)->users;
//
//        $collection = new Collection($subproject, $this->subprojectTransformer);
//
//        $data = $this->fractal->createData($collection)->toArray();
//
//        return $this->respond($data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$user = Auth::user();
        
        $user->hasRolesOrFail(Config::get('app.route_permissions.projects'));
        // validate
        $validate = Validator::make($request->all(), [
            'name' => 'required',
            'project_id' => 'required'
        ]);
        if ($validate->fails())
        {
            $this->setStatusCode(IlluminateResponse::HTTP_BAD_REQUEST);
            $item = new Item($validate, $this->validationTransformer);
        } 
        else 
        {
            $project = ProjectMember::where('user_id','=',$user->id)->where('project_id','=',$request->input('project_id'))->firstOrFail()->project;
            $subproject = new Subproject;
            $subproject->name = $request->input('name');
            $subproject->description = $request->input('description');
            $subproject->project_id = $project->id;
            
            if ($subproject->save())
            {
                $this->setStatusCode(IlluminateResponse::HTTP_CREATED);
                $item = new Item($subproject, $this->subprojectTransformer);
            } else {
                $this->setStatusCode(IlluminateResponse::HTTP_INTERNAL_SERVER_ERROR);
                $error = "The subproject was not saved";
                $item = new Item($error, $this->errorTransformer);
            }

        }
        $data = $this->fractal->createData($item)->toArray();
        return $this->respond($data);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $user = Auth::user();
        $subproject = Subproject::findOrFail($id);
        $project_id = $subproject->project->id;
		$project = ProjectMember::where('user_id','=',$user->id)->where('project_id','=',$project_id)->firstOrFail()->project;
        $item = new Item($subproject, $this->subprojectTransformer);
        $data = $this->fractal->createData($item)->toArray();

        return $this->respond($data);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$user = Auth::user();
        
        $user->hasRolesOrFail(Config::get('app.route_permissions.projects'));
        // validate
        $validate = Validator::make($request->all(), [
            'name' => 'required',
            'project_id' => 'required'
        ]);
        if ($validate->fails())
        {
            $this->setStatusCode(IlluminateResponse::HTTP_BAD_REQUEST);
            $item = new Item($validate, $this->validationTransformer);
        } 
        else 
        {   
            
            $project = ProjectMember::where('user_id','=',$user->id)->where('project_id','=',$request->input('project_id'))->firstOrFail()->project;
            $subproject = Subproject::findOrFail($id);
            $subproject->name = $request->input('name');
            $subproject->abbreviation = $request->input('description');
            $subproject->project_id = $project->id;
            
            if ($subproject->save())
            {
                $this->setStatusCode(IlluminateResponse::HTTP_CREATED);
                $item = new Item($subproject, $this->subprojectTransformer);
            } else {
                $this->setStatusCode(IlluminateResponse::HTTP_INTERNAL_SERVER_ERROR);
                $error = "The subproject was not saved";
                $item = new Item($error, $this->errorTransformer);
            }

        }
        $data = $this->fractal->createData($item)->toArray();
        return $this->respond($data);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$user = Auth::user();
        
        $user->hasRolesOrFail(Config::get('app.route_permissions.projects'));
        $subproject = Subproject::findOrFail($id);
        $project_id = $subproject->project->id;
        $project = ProjectMember::where('user_id','=',$user->id)->where('project_id','=',$project_id)->firstOrFail();


        if (!$subproject->delete())
        {
            $this->setStatusCode(IlluminateResponse::HTTP_INTERNAL_SERVER_ERROR);
            $error = "The subproject was not deleted";
            $item = new Item($error, $this->errorTransformer);
            $data = $this->fractal->createData($item)->toArray();
            return $this->respond($data);
        }
	}

}

<?php namespace App\Http\Controllers;

use Illuminate\Http\Response as IlluminateResponse;
use App\Http\Controllers\ApiController;
use Auth;
use App\ProjectMember;
use App\Transformers\ProjectTransformer;
use App\Transformers\ValidationTransformer;
use App\Transformers\ErrorTransformer; 
use Illuminate\Http\Request;  
use League\Fractal\Manager;  
use League\Fractal\Resource\Collection;  
use League\Fractal\Resource\Item;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Config;

class ProjectMemberController extends ApiController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$user = Auth::user();
        
        $user->hasRolesOrFail(Config::get('app.route_permissions.project_members'));
        // validate
        $validate = Validator::make($request->all(), [
            'project_id' => 'required',
            'user_id' => 'required',
        ]);
        if ($validate->fails())
        {
            $this->setStatusCode(IlluminateResponse::HTTP_BAD_REQUEST);
            $item = new Item($validate, $this->validationTransformer);
        } 
        else 
        {
            $project_member = new ProjectMember;
            $project_member->user_id = $request->input('user_id');
            $project_member->project_id = $request->input('project_id');
            if ($project_member->save())
            {
                $this->setStatusCode(IlluminateResponse::HTTP_CREATED);
                $item = new Item($project_member, $this->projectTransformer);
            } else {
                $this->setStatusCode(IlluminateResponse::HTTP_INTERNAL_SERVER_ERROR);
                $error = "The project was not saved";
                $item = new Item($error, $this->errorTransformer);
            }

        }
        $data = $this->fractal->createData($item)->toArray();
        return $this->respond($data);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}

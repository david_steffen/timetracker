<?php namespace App\Http\Controllers;

use Illuminate\Http\Response as IlluminateResponse;
use App\Http\Controllers\ApiController;
use Auth;
use App\Project; 
use App\ProjectMember;
use App\Transformers\ProjectTransformer;
use App\Transformers\ValidationTransformer;
use App\Transformers\ErrorTransformer; 
use Illuminate\Http\Request;  
use League\Fractal\Manager;  
use League\Fractal\Resource\Collection;  
use League\Fractal\Resource\Item;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Config;

class ProjectController extends ApiController {
    
    protected $fractal;
    protected $projectTransformer;
    protected $validationTransformer;
    protected $errorTransformer;


    public function __construct(Manager $fractal, ProjectTransformer $projectTransformer, 
                ValidationTransformer $validationTransformer, ErrorTransformer $errorTransformer) {
        $this->fractal = $fractal;
        $this->projectTransformer = $projectTransformer;
        $this->validationTransformer = $validationTransformer;
        $this->errorTransformer = $errorTransformer;
    }

    /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()  
    {
        $user = Auth::user();

        $projects = $user->findOrFail($user->id)->projects;

        $collection = new Collection($projects, $this->projectTransformer);

        $data = $this->fractal->createData($collection)->toArray();

        return $this->respond($data);
    }


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        
        $user = Auth::user();
        
        $user->hasRolesOrFail(Config::get('app.route_permissions.projects'));
        // validate
        $validate = Validator::make($request->all(), [
            'colour' => 'required',
            'abbreviation' => 'required|max:4',
            'name' => 'required'
        ]);
        if ($validate->fails())
        {
            $this->setStatusCode(IlluminateResponse::HTTP_BAD_REQUEST);
            $item = new Item($validate, $this->validationTransformer);
        } 
        else 
        {
            $project = new Project;
            $project->colour = $request->input('colour');
            $project->name = $request->input('name');
            $project->abbreviation = $request->input('abbreviation');
            $project->company = $request->input('company');
            if ($project->save())
            {
                $project->members()->save($user);
                $this->setStatusCode(IlluminateResponse::HTTP_CREATED);
                $item = new Item($project, $this->projectTransformer);
            } else {
                $this->setStatusCode(IlluminateResponse::HTTP_INTERNAL_SERVER_ERROR);
                $error = "The project was not saved";
                $item = new Item($error, $this->errorTransformer);
            }

        }
        $data = $this->fractal->createData($item)->toArray();
        return $this->respond($data);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)  
    {    
        $project = ProjectMember::where('user_id','=',Auth::user()->id)->where('project_id','=',$id)->firstOrFail()->project;

        $item = new Item($project, $this->projectTransformer);

        $data = $this->fractal->createData($item)->toArray();

        return $this->respond($data);
    }

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id,Request $request)
	{
        $user = Auth::user();
        
        $user->hasRolesOrFail(Config::get('app.route_permissions.projects'));
        // validate
        $validate = Validator::make($request->all(), [
            'colour' => 'required',
            'abbreviation' => 'required',
            'name' => 'required'
        ]);
        if ($validate->fails())
        {
            $this->setStatusCode(IlluminateResponse::HTTP_BAD_REQUEST);
            $item = new Item($validate, $this->validationTransformer);
        } 
        else 
        {
            $project = ProjectMember::where('user_id','=',$user->id)->where('project_id','=',$id)->firstOrFail()->project;
            $project->colour = $request->input('colour');
            $project->name = $request->input('name');
            $project->abbreviation = $request->input('abbreviation');
            $project->company = $request->input('company');
            if ($project->save())
            {
                $this->setStatusCode(IlluminateResponse::HTTP_CREATED);
                $item = new Item($project, $this->projectTransformer);
            } else {
                $this->setStatusCode(IlluminateResponse::HTTP_INTERNAL_SERVER_ERROR);
                $error = "The project was not updated";
                $item = new Item($error, $this->errorTransformer);
            }

        }
        $data = $this->fractal->createData($item)->toArray();
        return $this->respond($data);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $user = Auth::user();
        
        $user->hasRolesOrFail(Config::get('app.route_permissions.projects'));
        $project_member = ProjectMember::where('user_id','=',$user->id)->where('project_id','=',$id)->firstOrFail();
        $project = $project_member->project;

        if (!$project->delete() && !$project->members->detach($id))
        {
            $this->setStatusCode(IlluminateResponse::HTTP_INTERNAL_SERVER_ERROR);
            $error = "The project was not deleted";
            $item = new Item($error, $this->errorTransformer);
            $data = $this->fractal->createData($item)->toArray();
            return $this->respond($data);
        }
	}

}

<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class TimeLog extends Model {
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'time_logs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'project_id', 'time','description','log_date','subproject_id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at'];
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    public function project()
    {
        return $this->belongsTo('App\Project');
    }
    public function subproject()
    {
        return $this->belongsTo('App\Subproject');
    }
    

}
